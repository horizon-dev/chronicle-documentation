# Chronicle Platform Documentation

> A collection of documentation for the Chronicle Platform.

## Requirements

* Required
  * [Docker](https://www.docker.com/)
  * [Docker Compose](https://docs.docker.com/compose/)

## Building

To build, simply

```sh
./build.sh
```

## Release History

* In progess

## Meta

© 2017 - 2018 Horizon Digital Economy Research –
[www.horizon.ac.uk](https://www.horizon.ac.uk)

[https://bitbucket.org/horizon-dev/chronicle-documentation](
https://bitbucket.org/horizon-dev/chronicle-documentation)

## Contributing

1. Fork it (<https://bitbucket.org/horizon-dev/chronicle-documentation>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
