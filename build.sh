#!/bin/bash
export _USER=$(id -un)
export _GROUP=$(id -gn)
export _UID=$(id -u)
export _GID=$(id -g)
if [ -z $1 ]; then
    args="make"
elif [ "shell" == "$1" ]; then
    args=""
else
    args=$@
fi
docker-compose build --build-arg USER=$_USER --build-arg UID=$_UID \
    --build-arg GID=$_GID --build-arg GROUP=$_GROUP texlive
docker-compose run --rm texlive $args
