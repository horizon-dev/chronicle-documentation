import io
import logging
import os
import tarfile

import requests

"""
N.B. To get a refresh token initially, use:
curl -X POST -u "{client_key}:{client_secret}" \
      https://bitbucket.org/site/oauth2/access_token \
      -d grant_type=client_credentials
"""

__pdf_files = [
    '../id-server/id-server.pdf',
    '../timeline-server/timeline-server.pdf',
]

__html_dirs = {
    '../id-server/html': 'id-server',
    '../timeline-server/html': 'timeline-server',
}

__log_level = 'DEBUG'

logging.basicConfig(level=__log_level)
__logger = logging.getLogger(__name__)


def get_access_token(client_key, client_secret, refresh_token):
    r = requests.post(
        'https://bitbucket.org/site/oauth2/access_token',
        data={
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token,
        },
        auth=(client_key, client_secret)
    )
    return r.json()['access_token']


def upload(filename, repo, access_token):
    with open(filename, 'rb') as file:
        requests.post(
            'https://api.bitbucket.org/2.0/repositories/{}/downloads'
                    .format(repo),
            headers={'Authorization': 'Bearer {}'.format(access_token)},
            files={'files': file}
        )


def upload_blob(filename, blob, repo, access_token):
    with io.BytesIO(blob) as file:
        file.name = filename
        requests.post(
            'https://api.bitbucket.org/2.0/repositories/{}/downloads'
                    .format(repo),
            headers={'Authorization': 'Bearer {}'.format(access_token)},
            files={'files': file}
        )


def compress_dir(dir, dirname):
    buf = io.BytesIO()
    with tarfile.open(mode='x:bz2', fileobj=buf) as tf:
        tf.add(dir, arcname=dirname)
    return buf.getvalue()


def main(repo, client_key, client_secret, refresh_token):
    global __logger, __pdf_files, __html_dirs
    access_token = get_access_token(client_key, client_secret, refresh_token)
    for fn in __pdf_files:
        __logger.debug('Checking for file \'{}\''.format(fn))
        if os.path.isfile(fn):
            __logger.debug('Uploading file \'{}\''.format(fn))
            upload(fn, repo, access_token)
    for dn, fn in __html_dirs.items():
        __logger.debug('Checking for directory \'{}\''.format(dn))
        if os.path.islink(dn):
            dn = os.path.realpath(dn)
        if os.path.exists(dn):
            tarfile = compress_dir(dn, fn)
            upload_blob(
                    '{}.html.tar.bz2'.format(fn), tarfile, repo, access_token)


if __name__ == '__main__':
    repo = '{}/{}'.format(
            os.getenv('BITBUCKET_REPO_OWNER'),
            os.getenv('BITBUCKET_REPO_SLUG'))
    client_key = os.getenv('BITBUCKET_API_CLIENT_KEY')
    client_secret = os.getenv('BITBUCKET_API_CLIENT_SECRET')
    refresh_token = os.getenv('BITBUCKET_API_REFRESH_TOKEN')
    main(repo, client_key, client_secret, refresh_token)
