import logging
import os
import re
import sys

import requests

import asyncio
import asyncssh


__log_level = 'DEBUG'

__config = {
    'publish_server_hostname': os.getenv('PUBLISH_SERVER_HOSTNAME'),
    'publish_server_username': os.getenv('PUBLISH_SERVER_USERNAME'),
    'publish_dir': os.getenv('PUBLISH_DIR'),
    'html_package_urls': [
        (
            'timeline-server',
            'https://bitbucket.org/horizon-dev/chronicle-documentation/'
            'downloads/timeline-server.html.tar.bz2',
        ),
        (
            'id-server',
            'https://bitbucket.org/horizon-dev/chronicle-documentation/'
            'downloads/id-server.html.tar.bz2',
        )
    ]
}

logging.basicConfig(level=__log_level)
__logger = logging.getLogger(__name__)


async def main(config):
    global __logger
    async with asyncssh.connect(
        config['publish_server_hostname'],
        username=config['publish_server_username'],
        client_keys=[config['identity_file']]
    ) as conn:
        for (d, url) in config['html_package_urls']:
            fn = '{}.tar.bz2'.format(d)
            with open(fn, 'wb') as f:
                __logger.debug('Fetching \'{}\''.format(url))
                r = requests.get(url)
                f.write(r.content)
            uf = os.path.join(os.getcwd(), fn)
            await asyncssh.scp(uf, (conn, '.'))
            await conn.run(
                'tar -C {} -xf {}'.format(config['publish_dir'], fn),
                check=True)


if __name__ == '__main__':
    try:
        with open(os.path.join(os.getenv("HOME"), '.ssh/config')) as f:
            rx = re.compile(r'IdentityFile\s*(.*)')
            lines = f.readlines()
            for line in lines:
                m = rx.match(line)
                if m:
                    __config['identity_file'] = m.group(1)
                    break
        asyncio.get_event_loop().run_until_complete(main(__config))
    except (OSError, asyncssh.Error) as exc:
        sys.exit('SSH connection failed: ' + str(exc))
