FROM dominicjprice/texlive:ubuntu-18.04-os-2017
ARG USER
ARG GROUP
ARG UID
ARG GID
RUN groupadd -fg $GID $GROUP \
    && useradd -ms /bin/bash -u $UID -g $GROUP $USER
USER $USER:$GROUP
WORKDIR /documentation
