\documentclass[a4paper]{article}

\def\docversion{v0.0.7}
\def\serverversion{v0.0.3}
\def\serverdomain{timeline.chronicle.horizon.ac.uk}
\def\apiversion{v1}

\usepackage{appendix}
\ifdefined\theHchapter\else\newcommand\theHchapter{\Alph{chapter}}\fi
\ifdefined\theHsection\else\newcommand\theHsection{\Alph{section}}\fi
\usepackage{comment}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\hypersetup{
  hidelinks,
  pageanchor=false,
}
\usepackage[utf8]{inputenc}
\usepackage{minted}
\ifdefined\HCode
\else
\setminted{
  breaklines,
  breakafter=/
}
\fi
\usepackage{nameref}
\usepackage{textcomp}
\usepackage{tikz}
\usetikzlibrary{
  er,
  positioning,
}

\newcommand{\paragraphtitle}[1]{\paragraph{#1}\mbox{}\\}

\newcommand{\code}[1]{\texttt{#1}}

\makeatletter
\@ifpackageloaded{tex4ht}
  {\let\iftexforht\iftrue}
  {\let\iftexforht\iffalse}
\makeatother

\catcode`+=\active
\catcode`\!=\active
\newenvironment{escapedcode}[2]
{
  \VerbatimEnvironment
  \catcode`+=\active
  \catcode`\!=\active
  \def+dq+##1{\PYG{s}{\PYGZdq{}##1\PYGZdq{}}}
  \def!##1!{\csname ##1\endcsname}
  \begin{minted}[escapeinside=#2#2]{#1}}
{\end{minted}}
\catcode`+=12
\catcode`\!=12

\title{Chronicle Timeline Server}
\author{Horizon Digital Economy Research}
\date{\today}
\begin{document}

\begin{titlepage}
\maketitle{}
\thispagestyle{empty}
\begin{center}
Document Version: \docversion{}

Server Version: \serverversion{}

API Version: \apiversion{}
\end{center}
\end{titlepage}

\begin{abstract}
This document describes the Chronicle Timeline Server protocols, APIs and
reference implementation.
\end{abstract}
\tableofcontents{}
\listoffigures{}

\clearpage{}
\iffalse
\section{Introduction}

\clearpage{}

\section{Model}

See figure~\ref{fig:er}

\begin{figure}[h]
  \caption{Model Entity-Relationship}\label{fig:er}
  \centering
  \input{diagrams/model.tex}
\end{figure}

\clearpage{}

\section{User Interface}

\clearpage{}
\fi

\section{API}
\begin{itemize}
  \item Current Version: \apiversion{}
  \item Base Url: https://\serverdomain{}/api/\apiversion{}
\end{itemize}

\subsection{Authentication}

\subsubsection{User Authentication}

User authentication is achieved by signing a client request with an RSA private
key, the signature may then be verified by the server with the corresponding
public key. The basic user interface allows a user to either upload an RSA
public key, or to generate a new RSA key pair (the private key is not stored
on the server). Documentation on the basic user interface is pending.
User authentication information is passed in the \code{Authorization} header:

\begin{escapedcode}{bash}{`}
Authorization: chronicle-user-hmac
auth_nonce="",
auth_signature="",
auth_signature_method="RSA-SHA512",
auth_timestamp="",
auth_username="",
auth_version="1.0",
\end{escapedcode}

\begin{itemize}
  \item \code{auth\_nonce} - A nonce used by the server to protect against
  replay attacks. This can be any unique(ish) string though it is recommended
  to use a base64 encoded string of 32 random bytes.
  \item \code{auth\_signature} - See \nameref{sssec:gensig} for details on
  how this is generated.
  \item \code{auth\_signature\_method} - The method used to generate the
  signature, currently only \code{RSA-SHA512} is supported.
  \item \code{auth\_timestamp} - The current UTC time in seconds since the
  epoch.
  \item \code{auth\_username} - The username for the user authenticating the
  request.
  \item \code{auth\_version} - The authorization version, currently \code{1.0}.
\end{itemize}

\subsubsection{Token Authentication}
\label{sssec:tokauth}

Token authentication is achieved by signing a client request with a secret
key, the signature may then be verified by the server which maintains a copy
of the secret key. The basic user interface allows a user to generate tokens
that have grant the holder certain permissions (documentation pending).
Token authentication information is passed in the \code{Authorization} header:

\begin{escapedcode}{bash}{`}
Authorization: chronicle-token-hmac
auth_nonce="",
auth_signature="",
auth_signature_method="SHA512",
auth_timestamp="",
auth_token_id="",
auth_version="1.0",
\end{escapedcode}

\begin{itemize}
  \item \code{auth\_nonce} - A nonce used by the server to protect against
  replay attacks. This can be any unique(ish) string though it is recommended
  to use a base64 encoded string of 32 random bytes.
  \item \code{auth\_signature} - See \nameref{sssec:gensig} for details on
  how this is generated.
  \item \code{auth\_signature\_method} - The method used to generate the
  signature, currently only \code{RSA-SHA512} is supported.
  \item \code{auth\_timestamp} - The current UTC time in seconds since the
  epoch.
  \item \code{auth\_token\_id} - The ID of the token used for authenticating
  the request.
  \item \code{auth\_version} - The authorization version, currently \code{1.0}.
\end{itemize}

\paragraph{Token Scripts}\label{para:tokscr}
Tokens are associated with scripts which can be used to create more
complex authentication scenarios than just \textit{allow}. Scripts are written
in \code{Javascript} and are run after a request token has been validated. Two
functions are exposed to the script:

\begin{itemize}
  \item \code{authorize()} - This function should be called when the request
  should be authorized.
  \item \code{reject(message)} - This function should be called when the
  request should be rejected. It takes an optional \code{message} parameters
  which can be used to provided a message explaining the reason for the
  rejection.
\end{itemize}

\paragraph{}Only one of \code{authorize} or \code{reject} may be called in a
single script session, repeated calls will result in an exception. If neither is
called the default behaviour is to reject the request.

\paragraph{}It is possible for \textit{State} to be maintained between script
sessions. A variable named \code{state} is exposed to the script which contains
the current state. Changes to state are not automatically saved, in order to
save state the script must call the \code{update\_state(state)} function. State
must be JSON serializable and defaults to \code{null}.

\paragraph{}As a simple example, a script could be used to create a token that
could only be used a specific number of times. The below script, in combination
with a state set initially to \code{10} would only allow the token to be used
\textit{10} times.
\begin{escapedcode}{javascript}{`}
  if (state > 0) {
    update_state(state - 1);
    authorize();
  } else {
    reject('This token has expired');
  }
\end{escapedcode}

\paragraph{Token Permissions}\label{para:tokperms}
Access tokens must have permissions explicity assigned to them at the time
of creation, by default all permissions are set to false.

\paragraph{}The following permissions are available for Timeline access tokens:

\begin{itemize}
  \item Clone - Allows a Time timeline to be cloned, see API method
    \ref{sssec:clonetl} \nameref{sssec:clonetl}.
  \item Read - Allows a Timeline, including Entry metadata, to be read. See
    API method \ref{sssec:readtl} \nameref{sssec:readtl}.
  \item Add Entry - Allows an Entry to be added to a Timeline, see API method
    \ref{sssec:createtle} \nameref{sssec:createtle}.
  \item Entry Read - Allows Entries, including content, to be read from the
    Timeline. See API method \ref{sssec:readtle} \nameref{sssec:readtle}.
  \item Entry Edit - Allow an Entry to be updated, see API method
    \ref{sssec:updtent} \nameref{sssec:updtent}.
\end{itemize}

\paragraph{}See \ref{sssec:ctt} \nameref{sssec:ctt} for information on
creating Timeline access tokens.

\paragraph{}The following permissions are available for Timeline Entry access
tokens:

\begin{itemize}
  \item Entry Read - Allows Entry, including content, to be read. See API method
    \ref{sssec:readtle} \nameref{sssec:readtle}.
  \item Entry Edit - Allows Entry to be updated, see API method
    \ref{sssec:updtent} \nameref{sssec:updtent}.
\end{itemize}

\paragraph{}See \ref{sssec:ctet} \nameref{sssec:ctet} for information on
creating Timeline Entry access tokens.

\subsubsection{Generating a Signature}
\label{sssec:gensig}

The following method is used to generate a signature:

\begin{enumerate}
  \item Begin with an empty string.
  \item Append the request method (in uppercase).
  \item Append an ampersand (\&)
  \item \hyperlink{percenc}{Percent encode} the base request url (without query
  parameters or fragments) and append to the string.
  \item Append an ampersand (\&).
  \item Gather all of the url query parameters and all the auth\_ parameters.
  \item \hyperlink{percenc}{Percent encode} every key and every value.
  \item Sort alphabetically (ascending) on (encoded) key name,
  if there are duplicate entries for a key name, continue sorting on the
  (encoded) key value.
  \item Create a new empty string and, for each key/value pair:
  \begin{enumerate}
    \item Append the endoded key.
    \item Append an equals sign (=).
    \item Append the encoded value.
    \item If there are items remaining, append an ampersand (\&).
    \item Repeat until all key/value pairs are appended.
  \end{enumerate}
  \item \hyperlink{percenc}{Percent encode} the new query string and append.
  \item Append an ampersand (\&)
  \item \hyperlink{percenc}{Percent encode} the request body and append.
  \item \hyperlink{percenc}{Percent encode} the entire string and create
  a signature using the secret key (this is the RSA private key in the case
  of user authentication) and the SHA512 algorithm. PSS padding using the MGF1
  mask generation function should be used with the SHA512 algorithm. The
  signature should be \hyperlink{baseenc}{base64 encoded}.
\end{enumerate}

\hypertarget{percenc}{For more details} on percent encoding refer to
\href{https://tools.ietf.org/html/rfc3986.html\#section-2.1}{RFC 3986}.

\hypertarget{baseenc}{For more details} on base64 encoding refer to
\href{https://tools.ietf.org/html/rfc4648}{RFC 4648}.

\subsection{Methods}

\input{api-methods/create-timeline.tex}
\input{api-methods/create-timeline-token.tex}
\input{api-methods/read-timelines.tex}
\input{api-methods/read-timeline.tex}
\input{api-methods/delete-timeline.tex}
\input{api-methods/clone-timeline.tex}
\input{api-methods/create-timeline-entry.tex}
\input{api-methods/create-timeline-entry-chunked.tex}
\input{api-methods/create-timeline-entry-token.tex}
\input{api-methods/read-timeline-entries.tex}
\input{api-methods/read-timeline-entry.tex}
\input{api-methods/read-timeline-entry-content.tex}
\input{api-methods/update-timeline-entry.tex}

\clearpage{}

\appendix
\appendixpage
\addappheadtotoc

\input{appendices/revisions.tex}

\iftexforht
\href{/index.html}{Home} |
\href{/terms.html}{Terms \& Conditions} |
\href{/terms.html}{Privacy Policy} |
\href{/terms.html}{Cookie Policy}
\fi

\end{document}
